#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
	int A[100], dp[100];
	int n = 0;
	cin >> n;
	
	for (int i = 1; i <= n; i++)
	{
		cin >> A[i];
	}
	
	sort(A+1, A+n+1);
	dp[2] = A[2] - A[1];
	dp[3] = A[3] - A[1];
	
	for (int i = 4; i <= n; i++)
	{
		dp[i] = min(dp[i-1], dp[i-2]) + A[i] - A[i-1];
	}
	cout << dp[n];
}
