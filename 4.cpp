#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

void matrix_init(int A[], int M)
{
	for (int i = 0; i < M; i++)
		A[i] = rand()%35 + 10; 
}

void matrix_print(int A[], int M)
{
	for (int i = 0; i < M; i++)
		cout << A[i] << "\t"; 
}

int main()
{
	int A[100], B[100];
	int M = 0, N = 0;
	cin >> M >> N;
	
	matrix_init(A, M);
	matrix_init(B, N);
	
	matrix_print(A, M);
	cout << endl;
	matrix_print(B, N);
	
}
