#include <iostream>
#include <ctime>

using namespace std;

//���������� ������� �����
void ShellSort(int n, int mass[])
{
	int i, j, step;
	int tmp;
	for (step = n / 2; step > 0; step /= 2)
		for (i = step; i < n; i++)
		{
			tmp = mass[i];
			for (j = i; j >= step; j -= step)
			{
				if (tmp < mass[j - step])
					mass[j] = mass[j - step];
				else
					break;
			}
			mass[j] = tmp;
		}
}

int main()
{
	//���� N
	int N;
	cout << "Input N: ";
	cin >> N;
	//��������� ������ ��� ������
	int* mass = new int[N];
	//���� ��������� �������
	// ���������� ������� ���������� ������� � ����� �� �����
	srand(time(NULL));
	for (int i = 0; i < N; i++) {
		mass[i] = rand() % 201 - 100;
		cout << mass[i] << " ";
	}
	cout << endl;

	//���������� ������� �����
	ShellSort(N, mass);
	//����� ���������������� ������� �� �����
	cout << "Sorted array:\n";
	for (int i = 0; i < N; i++)
		cout << mass[i] << "  ";
	cout << endl;
	//������������ ������
	delete[] mass;
	return 0;
}
