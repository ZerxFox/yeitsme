#include <iostream>
#include <cmath>

using namespace std;

double f(double x)
{
	return ((sin(7.25 * x + x*x))/2 + x);
}

int main()
{
	const double PI = 3.1416;
	double Z = 0.0, max = 0.0;
	
	for (double x = 0.0; x <= (PI/2); x += (PI/12))
	{
		Z = f(x);
		if (Z >= max) max = Z;
	}
	cout << max;
	return 0;
}
