#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

double r, R;
double randFloat()
{
	return double(rand()) / RAND_MAX * (2 * R) - R;
}
bool insideFigure(double x, double y)
{
	return (x*x + y*y) >= r*r && (x*x + y*y) <= R*R && (x >= 0 || y >= 0);
}

int main()
{
	srand(time(0));
	double shot = 0.0;
	double dots = 1000000.0;
	const double PI = 3.1416;
	cin >> r >> R;
	
	for (int i = 0; i < dots; i++)
	{
		if(insideFigure(randFloat(), randFloat()))
		{
			shot++;
		}
	}
	
	double A = (4 * R * R * shot) / dots;
	double B = ((PI * R * R) - (PI * r * r)) * 0.75;
	cout << A << " " << B;
	
	return 0;
}
