#include <iostream>
#include <cmath>

using namespace std;

double f(double x)
{
	return (sin(3*x));
}

int main()
{
	double a, b;
	double dx, s;
	int n = 0;
	
	cin >> a >> b >> n;
	
	s = (f(a) + f(b))/2;
	dx = (b-a) * 1.0 / n;
	
	for (int i = 1; i <= n-1; i++)
	{
		s += f(a + i * dx);
	}
	s *= dx;
	cout << endl << s;
	
	return 0;
}
