#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

double f(double x)
{
	return (log(1+exp(x)) - log(2 + sin(x*x)/4) - x/2)/(pow(sin(x),2) * pow(tan(x),2));
}
int main()
{
	double a = 0, b = 0, step = 0;
	
	cin >> a >> b >> step;
	
	cout << "-----------------------" << endl;
	cout << "|  x  |      f(x)     |" << endl;
	cout << "-----------------------" << endl;
	
	for(double i = a; i <= b; i += step)
	{
		cout << '|' << setw(5) << i << "|" << setw(15) << f(i) << "|" << endl;
		cout << "-----------------------" << endl;
	}
	
	return 0;
}
