#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;

int main()
{
	srand(time(0));
	int A[10];
	for (int i = 0; i < 10; i++){
		A[i] = 7 + rand()%8;
		cout << A[i] << "\t"; 
	}
	
	cout << endl << endl;
	for (int i = 0; i < 10; i++){
		if(A[i] >= 10) A[i] -= 10;
		cout << A[i] << "\t"; 
	}
	
	return 0;
}
