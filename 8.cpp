#include <iostream>
#include <cmath>
using namespace std;

double fact(double n) {
	if (n < 0) 
		return 0;
	else if (n == 0) 
		return 1;
	else
		return n * fact(n - 1);
}

int main()
{
	int n = 0, num = 1;
	float x = 0.0, sum = 0.0;
	
	cin >> x >> n;
	
	sum = x;
	while (num <= n)
	{
		sum += sin(pow(x, num)) / fact(num);
		num++;
	}
	cout << sum;
	return 0;
}
