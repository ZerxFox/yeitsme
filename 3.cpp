#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

int main()
{
	srand(time(0));
	
	int A[100][100];
	int M = 0, N = 0;
	int sum[100];
	int row = 0;
	
	cin >> M >> N;
	
	for (int i = 0; i < M; i++)
	{
		for (int j = 0; j < N; j++)
		{
			A[i][j] = 10 + rand()%91;
			cout << A[i][j] << "\t";
		}
		cout << endl;
	}
	
	for (int i = 0; i < M; i++)
	{
		for (int j = 0; j < N; j++)
		{
			sum[i] += A[i][j];
		}
	}
	
	cout << endl;
	for (int i = 0; i < M; i++)
	{
		cout << i + 1 << " - " << sum[i] << endl;
	}
	
	for (int i = 1; i < M; i++)
	{
		if(sum[i] > sum[row]) row = i;
	}
	cout << endl << "max: " << row + 1;
	
	return 0;
	
}
