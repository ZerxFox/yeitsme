#include <iostream>
#include <cmath>

using namespace std;

int main ()
{
	int A[100];
	int N = 0, M = 1;
	
	cin >> N;
	
	for(int i = 1; i <= N; i++)
	{
		A[i] = i;
		M *= A[i];
	}
	
	double s = pow(M, 1./N);
	cout << s;
	
	return 0;
}
